# Usage
Before you use it, you need to have python. Here is Python's site: https://www.python.org/
After you install the Python, download this reposity. After downloading the reposity, go to
same directory with reposity and type "python pialph.py". It will wants a input like "Alphabet -> ".
Type your alphabet without spaces and press enter. It will creates the pi to alphabet.
# How it works
Let we say you typed a alphabet like 'abcçdefgğhıijklmnoöprstşuüvyz'. It will enumerate it and turn it
to a dictionary like:
Enumerating:
0 a
1 b
2 c
...
To dict:
{
0:'a',
1:'b',
2:'c',
...
}
And replacing digits of pi as using keys and replacing it with their values (charatchers).
Note: Let we say you typed 27 charatchers alphabet. On iterating, it will starts to replace
from 27 to 1. Not from 1 to 27.
