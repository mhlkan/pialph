import string
def movenum(d,n):
    nd = {}
    for a in d:
        nd[a+n] = d[a]
    return nd
alphabet = dict(enumerate(list(str(input('Alphabet -> ')))))
with open('pi.txt') as pif:
    pi = pif.read()
    for k, v in list(alphabet.items())[::-1]:
        pi = pi.replace(str(k), str(v))
print(pi)
